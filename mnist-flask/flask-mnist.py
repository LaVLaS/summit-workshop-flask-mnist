#!/usr/bin/env python3

import flask
import PIL
import PIL.ImageOps
import numpy as np
import os
import json
import requests


app = flask.Flask(__name__)


@app.route('/', methods=['GET'])
def index():
    return flask.render_template("mnist.html")


@app.route('/image', methods=['POST'])
def image():
    # Note, even though we do a little prep, we don't clean the image nearly
    # as well as the MNIST dataset expects, so there will be some issues with
    # generalizing it to handwritten digits

    # Start by taking the image into pillow so we can modify it to fit the
    # right size
    pilimage: PIL.Image.Image = PIL.Image.frombytes(
        mode="RGBA", size=(200, 200), data=flask.request.data)

    # Resize it to the right internal size
    pilimage = pilimage.resize((20, 20))

    # Need to replace the Alpha channel, since 0=black in PIL
    newimg = PIL.Image.new(mode="RGBA", size=(20, 20), color="WHITE")
    newimg.paste(im=pilimage, box=(0, 0), mask=pilimage)

    # Turn it from RGB down to grayscale
    grayscaled_image = PIL.ImageOps.grayscale(newimg)

    # Add the padding so we have it at 28x28, with the 4px padding on all sides
    padded_image = PIL.ImageOps.expand(
        image=grayscaled_image, border=4, fill=255)

    # Call Invert here, since Pillow assumes 0=black 255=white, and our neural
    # net assumes the opposite
    inverted_image = PIL.ImageOps.invert(padded_image)

    # Finally, convert our image to the (28, 28, 1) format expected by the
    # model. Tensorflow expects an array of inputs, so we end up with
    reshaped_image = np.array(
        list(inverted_image.tobytes())).reshape((1, 28, 28, 1))

    scaled_image_array = reshaped_image / 255.0

    # Now since we have our model being served via http, we can simply make
    # a request
    req = requests.post(os.environ["TF_SERVING_URL"] + '/v1/models/mnist-model:predict',
                        json={"instances": scaled_image_array.tolist()})

    # The predictions come back as an array, with the confidence in each
    # category, we take the max confidence as our result
    return str(np.array(req.json()["predictions"][0]).argmax())


if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=True)
