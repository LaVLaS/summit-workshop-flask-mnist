FROM fedora:29

RUN yum -y update && \
    yum -y install python3 python3-pip

# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /app/requirements.txt

ENV TF_SERVING_URL http://tf-serving:8501
EXPOSE 5000

WORKDIR /app

RUN pip3 install -r requirements.txt

COPY mnist-flask/ /app

ENTRYPOINT [ "python3" ]

CMD [ "flask-mnist.py" ]
